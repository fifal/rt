## Instalace
### Databáze
Ve složce `sql_scripts` je skript pro vytvoření databáze a naplnění databáze testovacími daty.

- Schéma databáze:
    - ![schema](https://data.filek.cz/uploads/db.PNG)

### Webová aplikace
1. `composer install`
2. `app/config/config.neon` přidat nastavení pro připojení k MySQL databázi
3. Hotovo :D

- Backend: 
    - práce se snippetama a Ajaxem: [nette.ajax.js](https://componette.com/vojtech-dobes/nette.ajax.js/).
- Frontend:
    - Bootstrap 4

## Používání
### Přehled projektů
Tabulku (snippet) je možné řadit podle sloupců (ID, Název, Datum, Typ, Webový projekt) kliknutím na název sloupce.
Podle kterého sloupce je tabulka aktuálně řazena je zvýrazněno podtržením daného sloupce, šipka značí typ řazení (ASC, DESC).

Projekty je možné upravovat nebo mazat, pomocí tlačítek ze sloupce možnosti. 
![prehled](https://data.filek.cz/uploads/prehled.PNG)

### Přidání / editování projektu
Pro přidání nebo editování projektu je připraven formulář se čtyřmi prvky, které jsou povinné 
(Název, datum odevzdání, typ projektu, checkbox webový projekt). V případě přidávání nového projekt 
je formulář prázdný, při úpravě existujícího projektu jsou pole předvyplněna.

Pole datum je kontrolováno pomocí regexu a datum musí být zadán ve formátu `dd.mm.YYYY`. 
Kontrola funguje jak pomocí javascriptu tak i bez něj v prohlížečích s podporou HTML5. Na serveru je 
hodnota také ověřována. 

Selectbox typ projektu je generován dynamicky podle položek v databázi v tabulce `TYP`. 

![pridat](https://data.filek.cz/uploads/pridat.PNG)