<?php
/**
 * Created by PhpStorm.
 * User: Fifal
 * Date: 04.06.2018
 * Time: 19:20
 */

namespace App\Model;

class Model
{
    /** @var \Nette\Database\Context  */
    private $database;

    public function __construct(\Nette\Database\Context $context)
    {
        $this->database = $context;
    }

    /**
     * Creates project in DB
     *
     * @param $data:
     *      typ_id => 0 (časově omezený), 1 (Continuous Integration)
     *      nazev => varchar(50)
     *      datum => date
     *      web => tinyint(1), 0 - false, jinak true
     * @return
     */
    public function createProject($data){
        return $this->database->table('PROJEKT')
            ->insert($data);
    }

    /**
     * Returns all projects in DB
     *
     * @return \Nette\Database\Table\Selection
     */
    public function getProjects(){
        return $this->database->table('PROJEKT');
    }

    /**
     * Updates project information
     *
     * @param $projectId: ID of project
     * @param $data: same as createProject3
     * @return
     */
    public function updateProject($projectId, $data){
        return $this->database->table('PROJEKT')
            ->where('id = ' . $projectId)
            ->update($data);
    }

    /**
     * Deletes project with given ID
     *
     * @param $projectId: project ID
     * @return
     */
    public function deleteProject($projectId){
        return $this->database->table('PROJEKT')
            ->where('id = ' . $projectId)
            ->delete();
    }

    /**
     * Retunrs all types
     *
     * @return \Nette\Database\Table\Selection
     */
    public function getTypes(){
        return $this->database->table('TYP');
    }
}