<?php

namespace App\Presenters;

use App\Components\TableComponent;
use App\Model\Model;
use Nette;
use Nette\Utils\DateTime;


class HomepagePresenter extends Nette\Application\UI\Presenter
{
    /** @var Model */
    private $model;

    public function __construct(Model $model)
    {
        parent::__construct();
        $this->model = $model;
    }

    //
    // RENDERS
    //
    //////////////////////////////////////////////////

    /**
     * Redirect to project list
     * @throws Nette\Application\AbortException
     */
    public function renderDefault()
    {
        $this->redirect('Homepage:listprojects');
    }

    /**
     * Redirect to project list if project ID for delete wasn't entered
     * @throws Nette\Application\AbortException
     */
    public function renderDeleteProject()
    {
        $this->flashMessage('Nebylo zadáno ID projektu ke smazání!', 'danger');
        $this->redirect('Homepage:listprojects');
    }

    /**
     * Edit of existing project
     *
     * @param $projectId
     * @throws Nette\Application\AbortException
     */
    public function renderEditProject($id)
    {
        if ($id == null || !is_numeric($id)) {
            $this->flashMessage('Nebylo zadáno ID projektu k editaci.', 'danger');
            $this->redirect('Homepage:listprojects');
        }

        $project = $this->model->getProjects()->where('id = ' . $id)->fetch();
        // If project with $projectId wasn't found
        if (!$project) {
            $this->flashMessage('Projekt s id ' . $id . ' neexistuje!', 'danger');
            $this->redirect('Homepage:listprojects');
        }

        // Fill form
        $form = $this->getComponent('editForm');
        $form->setDefaults([
            'nazev' => $project->nazev,
            'datum' => $this->formatDateFromMysql($project->datum),
            'typ_id' => $project->typ_id,
            'web' => $project->web
        ]);
    }

    //
    // ACTIONS
    //
    //////////////////////////////////////////////////

    /**
     * Deletes project from database
     *
     * @param $id
     * @throws Nette\Application\AbortException
     */
    public function actionDeleteProject($id)
    {
        if ($id == null || !is_numeric($id)) {
            $this->flashMessage('Projekt s id ' . $id . ' nebyl nalezen!', 'danger');
            $this->redirect('Homepage:listprojects');
        }

        $result = $this->model->deleteProject($id);

        // If project wasn't found
        if (!$result) {
            $this->flashMessage('Projekt s id ' . $id . ' nebyl nalezen!', 'danger');
            $this->redirect('Homepage:listprojects');
        }

        $this->flashMessage('Projekt byl smazán!', 'success');
        $this->redirect('Homepage:listprojects');

    }

    //
    // COMPONENTS
    //
    //////////////////////////////////////////////////

    /**
     * Table with sort options
     *
     * @return TableComponent
     */
    protected function createComponentTableComponent()
    {
        return new TableComponent($this->model);
    }

    /**
     * Form for adding new projects
     *
     * @return Nette\Application\UI\Form
     */
    protected function createComponentProjectForm()
    {
        $form = (new \ProductFormFactory())->create();

        $items = $this->getProjectTypes();
        $form['typ_id']->items = $items;

        $form->addSubmit('send', 'Vytvořit projekt')
            ->setHtmlAttribute('class', 'form-control btn btn-success');
        $form->onSuccess[] = [$this, 'projectFormSuccess'];

        return $form;
    }

    /**
     * Form for adding projects successfully received
     *
     * @param Nette\Application\UI\Form $form
     * @param $values
     * @throws Nette\Application\AbortException,
     */
    public function projectFormSuccess(Nette\Application\UI\Form $form, $values)
    {
        // Check for required values, if not exists => redirect to add project
        if (count($values) != 4 && !isset($values['nazev']) && !isset($values['datum']) && !isset($values['typ_id']) && !isset($values['web'])) {
            $this->flashMessage('POST request neobsahoval všechny povinné pole.', 'danger');
            $this->redirect('Homepage:addproject');
        }

        // Check for date format
        try {
            $date = DateTime::createFromFormat('d.m.Y', $values['datum']);
            $dateString = $date->format('Y.m.d');
        } catch (\Exception $ex) {
            $this->flashMessage('Datum bylo zadáno ve špatném formátu.', 'danger');
            $this->redirect('Homepage:addproject');
        }

        $web = $values['web'] ? 1 : 0;

        $data = array('nazev' => $values['nazev'],
            'datum' => $dateString,
            'typ_id' => $values['typ_id'],
            'web' => $web
        );

        $result = $this->model->createProject($data);

        if (!$result) {
            $this->flashMessage('Nastala chyba při vkládání formuláře!', 'danger');
        } else {
            $this->flashMessage('Projekt byl vytvořen!', 'success');
        }
    }

    /**
     * Form for editing projects
     *
     * @return Nette\Application\UI\Form
     */
    protected function createComponentEditForm()
    {
        $form = (new \ProductFormFactory())->create();
        $form->addSubmit('send', 'Upravit projekt')
            ->setHtmlAttribute('class', 'form-control btn btn-success');

        $items = $this->getProjectTypes();
        $form['typ_id']->items = $items;

        $form->onSuccess[] = [$this, 'editFormSuccess'];

        return $form;
    }

    /**
     * Edit form successfully received
     *
     * @param Nette\Application\UI\Form $form
     * @param $values
     * @throws Nette\Application\AbortException
     */
    public function editFormSuccess(Nette\Application\UI\Form $form, $values)
    {
        $projectId = $this->getParameter('id');

        // If required parameters doesn't exist => redirect to edit project
        if (count($values) != 4
            && !isset($values['nazev'])
            && !isset($values['datum'])
            && !isset($values['typ_id'])
            && !isset($values['web'])
            && !isset($projectId)
        ) {
            $this->flashMessage('POST request neobsahoval všechny povinné pole.', 'danger');
            $this->redirect('Homepage:editproject', $projectId);
        }

        try {
            $values['datum'] = $this->formatDateToMysql($values['datum']);
        } catch (\Exception $ex) {
            $this->flashMessage('Datum bylo zadáno ve špatném formátu.', 'danger');
            $this->redirect('Homepage:editproject', $projectId);
        };

        $result = $this->model->updateProject($projectId, $values);

        if (!$result) {
            // If database sql returns 0 => same values as before update
            $this->flashMessage('Nastala chyba při upravování projektu. ', 'danger');
        } else {
            $this->flashMessage('Projekt byl úspěšně upraven.', 'success');
        }
    }

    //
    // UTILS
    //
    //////////////////////////////////////////////////

    /**
     * Formats date from MySQL format Y-m-d H:i:s to d.m.Y
     *
     * @param $mysqlDate
     * @return string
     */
    private function formatDateFromMysql($mysqlDate)
    {
        $date = DateTime::createFromFormat('Y-m-d H:i:s', $mysqlDate);
        return $date->format('d.m.Y');
    }

    /**
     * Formats date from d.m.Y to MySQL format Y-m-d
     *
     * @param $date
     * @return string
     */
    private function formatDateToMysql($date)
    {
        $date = DateTime::createFromFormat('d.m.Y', $date);
        return $date->format('Y-m-d');
    }

    /**
     * Returns project types in associative array
     *
     * @return array: [typ_id => typ_nazev]
     */
    private function getProjectTypes()
    {
        $items = array();
        $types = $this->model->getTypes()->fetchAll();

        foreach ($types as $type) {
            $items[$type->id] = $type->nazev;
        }

        return $items;
    }
}
